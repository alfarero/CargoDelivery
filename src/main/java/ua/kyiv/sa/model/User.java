package ua.kyiv.sa.model;

import java.util.ArrayList;
import java.util.List;

public class User extends Person {
    private long id;
    private List<Address> addressList = new ArrayList<>();
    private List<Account> accounts = new ArrayList<>();
    private List<Order> orders = new ArrayList<>();
    private List<Cargo> cargoList = new ArrayList<>();
    private List<Service> history = new ArrayList<>();
    private List<Request> requests = new ArrayList<>();
    private String password;
    private String login;

}
