package ua.kyiv.sa.model;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Account {
    long id;
    String name;
    LocalDate expireDate;
    BigDecimal balance;
    long userId;
}
