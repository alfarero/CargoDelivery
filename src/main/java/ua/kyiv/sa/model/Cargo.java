package ua.kyiv.sa.model;

import java.time.LocalDateTime;

public class Cargo {
    private long id;
    private CargoType type;
    private CargoParameters cargoParameters;
    private LocalDateTime receivedDate;
    private CargoStatus status;
    private Rate rateId;
}
