package ua.kyiv.sa.model;

import java.math.BigDecimal;

public class Rate {
    long id;
    BigDecimal rate;
    Currency currency;
    CargoUnit unit;
}
