package ua.kyiv.sa.model;

import java.math.BigDecimal;
import java.util.List;

public class Order {

    long id;
    List<Cargo> cargos;
    long userId;
    BigDecimal sum;
    Currency currency;

}
