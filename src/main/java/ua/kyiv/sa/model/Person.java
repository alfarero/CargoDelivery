package ua.kyiv.sa.model;

import java.time.LocalDateTime;
import java.util.Date;

public class Person {

    private String surname;
    private String name;
    private String fatherName;
    private String shortInfo;
    private String nickname;
    private String comment;
    private int homePhone;
    private int mobilePhone1;
    private int mobilePhone2;
    private String email;
    private String skypeLogin;
    private Address address;
    private LocalDateTime creationDate;
    private LocalDateTime lastEditedDate;


}
